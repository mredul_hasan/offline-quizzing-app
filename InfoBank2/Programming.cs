﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Media;
using System.Threading;

namespace InfoBank2
{

    public partial class Programming : Form
    {
        public string name;
        public string gender;


        public int wrong = 0, right = 0, answered = 0;
        public double number = 0.0;
        public string question, answer;
        public int questionleft = 10;
        public int time_seond = 120, min, sec ;
        public double perform;

        int[] ch = new int[30] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        Random rnd = new Random();
        int id ;

        // Grade Show

        Gread g = new Gread();

        private OleDbConnection connect = new OleDbConnection();
        public Programming()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=InfoBank.accdb;Persist Security Info=False;";
        
        }

        private void Programming_Load(object sender, EventArgs e)
        {

        }

        public void btn_start_Click(object sender, EventArgs e)
        {

            timer1.Start();
            lblCategory.Text = "Programming";

            try
            {
                connect.Open();


                id = rnd.Next(1, 25);
                ch[id] = 1;

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = connect;
                string query = "select * from Programming where [ID] = " + id.ToString();
                cmd.CommandText = query;

                OleDbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    radioButton1.Text = reader["Option1"].ToString();
                    radioButton2.Text = reader["Option2"].ToString();
                    radioButton3.Text = reader["Option3"].ToString();
                    radioButton4.Text = reader["Option4"].ToString();



                    question = reader["Question"].ToString();
                    answer = reader["Answer"].ToString();

                }

                questionTextBox.Text = " " + question.ToString();

                int ans = rnd.Next(1, 1000);
                if (ans % 4 == 0)
                {
                    radioButton1.Text = answer.ToString();

                }
                else if (ans % 4 == 1)
                {
                    radioButton2.Text = answer.ToString();

                }
                else if (ans % 4 == 3)
                {
                    radioButton4.Text = answer.ToString();

                }
                else if (ans % 4 == 2)
                {
                    radioButton3.Text = answer.ToString();

                }
                connect.Close();



                lblQusLft.Text = " " + (questionleft - 1);
               // questionleft--;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem " + ex.ToString());
            }

            btn_skip.Show();
            btn_submit.Show();

            btn_start.Hide();
        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true || radioButton2.Checked == true || radioButton3.Checked == true || radioButton4.Checked == true)
            {
                if (radioButton1.Checked == true)
                {
                    if (radioButton1.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton1.ForeColor = Color.LimeGreen;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();

                        Thread.Sleep(2000);
                        radioButton1.ForeColor = Color.LightSeaGreen;

                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton1.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Thread.Sleep(2000);

                        radioButton1.ForeColor = Color.LightSeaGreen;

                    }
                    answered++;
                }
                else if (radioButton2.Checked == true)
                {
                    if (radioButton2.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton2.ForeColor = Color.LimeGreen;


                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
                
                        Thread.Sleep(2000);
                        radioButton2.ForeColor = Color.LightSeaGreen;
                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton2.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Thread.Sleep(2000);
             
                        radioButton2.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }
                else if (radioButton3.Checked == true)
                {
                    if (radioButton3.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton3.ForeColor = Color.LimeGreen;


                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
        
                        Thread.Sleep(2000);
                        radioButton3.ForeColor = Color.LightSeaGreen;
                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton3.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Thread.Sleep(2000);
                    
                        radioButton3.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }
                else if (radioButton4.Checked == true)
                {
                    if (radioButton4.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton4.ForeColor = Color.LimeGreen;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
              
                        Thread.Sleep(2000);
                        radioButton4.ForeColor = Color.LightSeaGreen;
                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton4.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Thread.Sleep(2000);
             
                        radioButton4.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }
                else
                {
                    answered++;
                }

                radioButton1.Checked = false;
                radioButton2.Checked = false;
                radioButton3.Checked = false;
                radioButton4.Checked = false;

                try
                {
                    connect.Open();

                    while (true)
                    {
                        id = rnd.Next(1, 25);

                        if (ch[id] == 0)
                        {
                            ch[id] = 1;
                            break;
                        }
                    }

                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = connect;
                    string query = "select * from Programming where [ID] = " + id.ToString();
                    cmd.CommandText = query;

                    OleDbDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        radioButton1.Text = reader["Option1"].ToString();
                        radioButton2.Text = reader["Option2"].ToString();
                        radioButton3.Text = reader["Option3"].ToString();
                        radioButton4.Text = reader["Option4"].ToString();

                        question = reader["Question"].ToString();
                        answer = reader["Answer"].ToString();

                    }

                    questionTextBox.Text = " " + question.ToString();

                    int ans = rnd.Next(1, 1000);
                    if (ans % 4 == 0)
                    {
                        radioButton1.Text = answer.ToString();

                    }
                    else if (ans % 4 == 1)
                    {
                        radioButton2.Text = answer.ToString();

                    }
                    else if (ans % 4 == 3)
                    {
                        radioButton4.Text = answer.ToString();

                    }
                    else if (ans % 4 == 2)
                    {
                        radioButton3.Text = answer.ToString();

                    }
                    connect.Close();

                    questionleft--;
                    if (questionleft == 0)
                    {
                        timer1.Stop();
                        time_seond = 120 - time_seond;
                        min = time_seond / 60;
                        sec = time_seond % 60;

                        if (number < 0)
                        {
                            number = 0;
                        }

                        perform = (number / 20) * 100;


                        g.num = number;
                        g.answered = this.answered;
                        g.right = this.right;
                        g.Wrong = this.wrong;
                        g.performmance = perform;
                        g.time_min = min;
                        g.time_sec = sec;

                        g.name = this.name;
                        g.gender = this.gender;

                        this.Hide();
                        g.Show();
                    }

                    lblQusLft.Text = " " + (questionleft - 1);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem " + ex.ToString());
                }
            }
            else 
            {
                MessageBox.Show("Please,select any one of them...!");
            }
        }

        private void btn_skip_Click(object sender, EventArgs e)
        {
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            radioButton4.Checked = false;

            try
            {
                connect.Open();

                while (true)
                {
                    id = rnd.Next(1, 25);

                    if (ch[id] == 0)
                    {
                        ch[id] = 1;
                        break;
                    }
                }


                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = connect;
                string query = "select * from Programming where [ID] = " + id.ToString();
                cmd.CommandText = query;

                OleDbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    radioButton1.Text = reader["Option1"].ToString();
                    radioButton2.Text = reader["Option2"].ToString();
                    radioButton3.Text = reader["Option3"].ToString();
                    radioButton4.Text = reader["Option4"].ToString();



                    question = reader["Question"].ToString();
                    answer = reader["Answer"].ToString();

                }

                questionTextBox.Text = " " + question.ToString();

                int ans = rnd.Next(1, 1000);
                if (ans % 4 == 0)
                {
                    radioButton1.Text = answer.ToString();

                }
                else if (ans % 4 == 1)
                {
                    radioButton2.Text = answer.ToString();

                }
                else if (ans % 4 == 3)
                {
                    radioButton4.Text = answer.ToString();

                }
                else if (ans % 4 == 2)
                {
                    radioButton3.Text = answer.ToString();

                }
                connect.Close();

                questionleft--;
                if (questionleft == 0)
                {
                    timer1.Stop();
                    time_seond = 120 - time_seond;
                    min = time_seond / 60;
                    sec = time_seond % 60;

                    if (number < 0)
                    {
                        number = 0;
                    }

                    perform = (number / 20) * 100;


                    g.num = number;
                    g.answered = this.answered;
                    g.right = this.right;
                    g.Wrong = this.wrong;
                    g.performmance = perform;
                    g.time_min = min;
                    g.time_sec = sec;

                    g.name = this.name;
                    g.gender = this.gender;

                    this.Hide();
                    g.Show();
                }

                lblQusLft.Text = " " + (questionleft - 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem " + ex.ToString());
            }
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
          
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lb_time_Click(object sender, EventArgs e)
        {

        }

        private void categoryTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lb_category_Click(object sender, EventArgs e)
        {

        }

        private void lb_quesLeft_Click(object sender, EventArgs e)
        {

        }

        private void timeTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void remainQuesTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void questionTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_hint_Click(object sender, EventArgs e)
        {

        }

        private void hintTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time_seond = time_seond - 1;
            while (time_seond == 0)
            {
                timer1.Stop();
                time_seond = 120 - time_seond;
                min = time_seond / 60;
                sec = time_seond % 60;

                if (number < 0)
                {
                    number = 0;
                }

                perform = (number / 20) * 100;


                g.num = number;
                g.answered = this.answered;
                g.right = this.right;
                g.Wrong = this.wrong;
                g.performmance = perform;
                g.time_min = min;
                g.time_sec = sec;

                g.name = this.name;
                g.gender = this.gender;

                this.Hide();
                g.Show();
            }
            min = time_seond / 60;
            sec = time_seond % 60;
            
            
            timelvl.Text = min + " : " + sec +   " min." ;
        }

        private void timelvl_Click(object sender, EventArgs e)
        {

        }

        private void mainMaunBtn_Click(object sender, EventArgs e)
        {
            Check_Grade cg = new Check_Grade();
            

            timer1.Stop();
            time_seond = 120 - time_seond;
            min = time_seond / 60;
            sec = time_seond % 60;

            if (number < 0)
            {
                number = 0;
            }

            perform = (number / 20) * 100;

            cg.num = number;
            cg.answered = this.answered;
            cg.right = this.right;
            cg.Wrong = this.wrong;
            cg.performmance = perform;
            cg.time_min = min;
            cg.time_sec = sec;
            cg.category = lblCategory.Text;


            cg.name = this.name;
            cg.gender = this.gender;


            this.Hide();
            cg.Show();

            
        }

        private void lblQusLft_Click(object sender, EventArgs e)
        {

        }

        private void lblCategory_Click(object sender, EventArgs e)
        {

        }
    }
}

