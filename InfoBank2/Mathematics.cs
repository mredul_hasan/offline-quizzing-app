﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Media;
using System.Threading;

namespace InfoBank2
{
    public partial class Mathematics : Form
    {
        public string name;
        public string gender;


        int wrong = 0, right = 0, answered = 0;
        double number = 0.0;
        string question, answer;
        int questionleft = 10;
        int time_seond = 120, min, sec;
        double perform;
        int ans;

        int[] ch = new int[60] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Random rnd = new Random();
        int id;

        Gread g = new Gread();

        private OleDbConnection connect = new OleDbConnection();
        public Mathematics()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=InfoBank.accdb;Persist Security Info=False;";
        }

        private void Mathematics_Load(object sender, EventArgs e)
        {

        }

        private void btn_start_Click(object sender, EventArgs e)
        {

            timer1.Start();
            lblCategory.Text = "Mathematics";


            try
            {
                connect.Open();

                id = rnd.Next(1, 20);
                ch[id] = 1;
                

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = connect;
                string query = "select * from Mathematics where [ID] = " + id.ToString();
                cmd.CommandText = query;

                OleDbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    radioButton1.Text = reader["Option1"].ToString();
                    radioButton2.Text = reader["Option2"].ToString();
                    radioButton3.Text = reader["Option3"].ToString();
                    radioButton4.Text = reader["Option4"].ToString();



                    question = reader["Question"].ToString();
                    answer = reader["Answer"].ToString();

                }

                
                    int[] operand = new int[30];
                    int i = 0, k = 0;
                    String s;

                    while(i < 30)
                    {
                        operand[i] = rnd.Next(1, 50);
                        i++;
                    }
                    
                    

                    // For answer

                    char[] c = question.ToCharArray();
                    String s1 = question;

                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    questionTextBox.Text = s1 + " = ?";

                    
                    answer = evaluateNew(s1).ToString();


                    // For radio button 1

                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton1.Text = evaluateNew(s1).ToString();

                    // For radio button 2

                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton2.Text = evaluateNew(s1).ToString();

                    // For radio button 3
                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton3.Text = evaluateNew(s1).ToString();


                    // For radio button 4
                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton4.Text = evaluateNew(s1).ToString();

                    ans = rnd.Next(1, 1000);
                    if (ans % 4 == 0)
                    {
                        radioButton1.Text = answer.ToString();

                    }
                    else if (ans % 4 == 1)
                    {
                        radioButton2.Text = answer.ToString();

                    }
                    else if (ans % 4 == 3)
                    {
                        radioButton4.Text = answer.ToString();

                    }
                    else if (ans % 4 == 2)
                    {
                        radioButton3.Text = answer.ToString();

                    }

               
         
                connect.Close();



                lblQusLft.Text = " " + (questionleft - 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem " + ex.ToString());
            }

            btn_skip.Show();
            btn_submit.Show();

            btn_start.Hide();
        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true || radioButton2.Checked == true || radioButton3.Checked == true || radioButton4.Checked == true)
            {
                if (radioButton1.Checked == true)
                {
                    if (radioButton1.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton1.ForeColor = Color.LimeGreen;


                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
                        Task.Delay(2000);
                        radioButton1.ForeColor = Color.LightSeaGreen;

                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton1.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Task.Delay(2000);
                        radioButton1.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }
                else if (radioButton2.Checked == true)
                {
                    if (radioButton2.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton2.ForeColor = Color.LimeGreen;


                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
                        Task.Delay(2000);
                        radioButton2.ForeColor = Color.LightSeaGreen;
                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton2.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Task.Delay(2000);
                        radioButton2.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }
                else if (radioButton3.Checked == true)
                {
                    if (radioButton3.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton3.ForeColor = Color.LimeGreen;


                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
                        Task.Delay(2000);
                        radioButton3.ForeColor = Color.LightSeaGreen;
                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton3.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Task.Delay(2000);
                        radioButton3.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }
                else if (radioButton4.Checked == true)
                {
                    if (radioButton4.Text == answer.ToString())
                    {
                        right++;
                        number = number + 2;
                        radioButton4.ForeColor = Color.LimeGreen;


                        SoundPlayer sndPing = new SoundPlayer(@"sound\right.wav");
                        sndPing.Play();
                        Task.Delay(2000);
                        radioButton4.ForeColor = Color.LightSeaGreen;
                    }
                    else
                    {
                        wrong++;
                        number = number - 0.5;
                        radioButton4.ForeColor = Color.Red;

                        SoundPlayer sndPing = new SoundPlayer(@"sound\wrong.wav");
                        sndPing.Play();

                        Task.Delay(2000);
                        radioButton4.ForeColor = Color.LightSeaGreen;
                    }
                    answered++;
                }


                radioButton1.Checked = false;
                radioButton2.Checked = false;
                radioButton3.Checked = false;
                radioButton4.Checked = false;

                try
                {
                    connect.Open();

                    while (true)
                    {
                        id = rnd.Next(1, 20);

                        if (ch[id] == 0)
                        {
                            ch[id] = 1;
                            break;
                        }
                    }


                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = connect;
                    string query = "select * from Mathematics where [ID] = " + id.ToString();
                    cmd.CommandText = query;

                    OleDbDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        radioButton1.Text = reader["Option1"].ToString();
                        radioButton2.Text = reader["Option2"].ToString();
                        radioButton3.Text = reader["Option3"].ToString();
                        radioButton4.Text = reader["Option4"].ToString();



                        question = reader["Question"].ToString();
                        answer = reader["Answer"].ToString();

                    }

                    int[] operand = new int[30];
                    int i = 0, k = 0;
                    String s;

                    while (i < 30)
                    {
                        operand[i] = rnd.Next(1, 50);
                        i++;
                    }



                    // For answer

                    char[] c = question.ToCharArray();
                    String s1 = question;

                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    questionTextBox.Text = s1 + " = ?";


                    answer = evaluateNew(s1).ToString();


                    // For radio button 1

                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton1.Text = evaluateNew(s1).ToString();

                    // For radio button 2

                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton2.Text = evaluateNew(s1).ToString();

                    // For radio button 3
                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton3.Text = evaluateNew(s1).ToString();


                    // For radio button 4
                    s1 = question;
                    for (i = 0; i < c.Length; i++)
                    {
                        if (c[i] == 'A')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("A", s);
                        }
                        else if (c[i] == 'B')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("B", s);
                        }
                        else if (c[i] == 'C')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("C", s);
                        }
                        else if (c[i] == 'D')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("D", s);
                        }
                        else if (c[i] == 'E')
                        {
                            s = operand[k++].ToString();
                            s1 = s1.Replace("E", s);
                        }
                    }
                    radioButton4.Text = evaluateNew(s1).ToString();

                    ans = rnd.Next(1, 1000);
                    if (ans % 4 == 0)
                    {
                        radioButton1.Text = answer.ToString();

                    }
                    else if (ans % 4 == 1)
                    {
                        radioButton2.Text = answer.ToString();

                    }
                    else if (ans % 4 == 3)
                    {
                        radioButton4.Text = answer.ToString();

                    }
                    else if (ans % 4 == 2)
                    {
                        radioButton3.Text = answer.ToString();

                    }
                    connect.Close();

                    questionleft--;
                    if (questionleft == 0)
                    {
                        timer1.Stop();
                        time_seond = 120 - time_seond;
                        min = time_seond / 60;
                        sec = time_seond % 60;

                        if (number < 0)
                        {
                            number = 0;
                        }

                        perform = (number / 20) * 100;


                        g.num = number;
                        g.answered = this.answered;
                        g.right = this.right;
                        g.Wrong = this.wrong;
                        g.performmance = perform;
                        g.time_min = min;
                        g.time_sec = sec;

                        g.name = this.name;
                        g.gender = this.gender;

                        this.Hide();
                        g.Show();



                    }

                    lblQusLft.Text = " " + (questionleft - 1);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem " + ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please select any one of them...!\n\nAnd if you want to avoid this question , just click on 'SKIP' button...!");
            }
        }

        private void btn_skip_Click(object sender, EventArgs e)
        {

            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            radioButton4.Checked = false;

            try
            {
                connect.Open();


                while (true)
                {
                    id = rnd.Next(1, 20);

                    if (ch[id] == 0)
                    {
                        ch[id] = 1;
                        break;
                    }
                }


                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = connect;
                string query = "select * from Mathematics where [ID] = " + id.ToString();
                cmd.CommandText = query;

                OleDbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    radioButton1.Text = reader["Option1"].ToString();
                    radioButton2.Text = reader["Option2"].ToString();
                    radioButton3.Text = reader["Option3"].ToString();
                    radioButton4.Text = reader["Option4"].ToString();



                    question = reader["Question"].ToString();
                    answer = reader["Answer"].ToString();

                }

                int[] operand = new int[30];
                int i = 0, k = 0;
                String s;

                while (i < 30)
                {
                    operand[i] = rnd.Next(1, 50);
                    i++;
                }



                // For answer

                char[] c = question.ToCharArray();
                String s1 = question;

                for (i = 0; i < c.Length; i++)
                {
                    if (c[i] == 'A')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("A", s);
                    }
                    else if (c[i] == 'B')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("B", s);
                    }
                    else if (c[i] == 'C')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("C", s);
                    }
                    else if (c[i] == 'D')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("D", s);
                    }
                    else if (c[i] == 'E')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("E", s);
                    }
                }
                questionTextBox.Text = s1 + " = ?";


                answer = evaluateNew(s1).ToString();


                // For radio button 1

                s1 = question;
                for (i = 0; i < c.Length; i++)
                {
                    if (c[i] == 'A')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("A", s);
                    }
                    else if (c[i] == 'B')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("B", s);
                    }
                    else if (c[i] == 'C')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("C", s);
                    }
                    else if (c[i] == 'D')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("D", s);
                    }
                    else if (c[i] == 'E')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("E", s);
                    }
                }
                radioButton1.Text = evaluateNew(s1).ToString();

                // For radio button 2

                s1 = question;
                for (i = 0; i < c.Length; i++)
                {
                    if (c[i] == 'A')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("A", s);
                    }
                    else if (c[i] == 'B')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("B", s);
                    }
                    else if (c[i] == 'C')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("C", s);
                    }
                    else if (c[i] == 'D')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("D", s);
                    }
                    else if (c[i] == 'E')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("E", s);
                    }
                }
                radioButton2.Text = evaluateNew(s1).ToString();

                // For radio button 3
                s1 = question;
                for (i = 0; i < c.Length; i++)
                {
                    if (c[i] == 'A')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("A", s);
                    }
                    else if (c[i] == 'B')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("B", s);
                    }
                    else if (c[i] == 'C')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("C", s);
                    }
                    else if (c[i] == 'D')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("D", s);
                    }
                    else if (c[i] == 'E')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("E", s);
                    }
                }
                radioButton3.Text = evaluateNew(s1).ToString();


                // For radio button 4
                s1 = question;
                for (i = 0; i < c.Length; i++)
                {
                    if (c[i] == 'A')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("A", s);
                    }
                    else if (c[i] == 'B')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("B", s);
                    }
                    else if (c[i] == 'C')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("C", s);
                    }
                    else if (c[i] == 'D')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("D", s);
                    }
                    else if (c[i] == 'E')
                    {
                        s = operand[k++].ToString();
                        s1 = s1.Replace("E", s);
                    }
                }
                radioButton4.Text = evaluateNew(s1).ToString();

                ans = rnd.Next(1, 1000);
                if (ans % 4 == 0)
                {
                    radioButton1.Text = answer.ToString();

                }
                else if (ans % 4 == 1)
                {
                    radioButton2.Text = answer.ToString();

                }
                else if (ans % 4 == 3)
                {
                    radioButton4.Text = answer.ToString();

                }
                else if (ans % 4 == 2)
                {
                    radioButton3.Text = answer.ToString();

                }
                connect.Close();

                questionleft--;
                if (questionleft == 0)
                {
                    timer1.Stop();
                    time_seond = 120 - time_seond;
                    min = time_seond / 60;
                    sec = time_seond % 60;

                    if (number < 0)
                    {
                        number = 0;
                    }

                    perform = (number / 20) * 100;


                    g.num = number;
                    g.answered = this.answered;
                    g.right = this.right;
                    g.Wrong = this.wrong;
                    g.performmance = perform;
                    g.time_min = min;
                    g.time_sec = sec;

                    g.name = this.name;
                    g.gender = this.gender;

                    this.Hide();
                    g.Show();
                }

                lblQusLft.Text = " " + (questionleft - 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem " + ex.ToString());
            }
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time_seond = time_seond - 1;
            while (time_seond == 0)
            {

                timer1.Stop();
                time_seond = 120 - time_seond;
                min = time_seond / 60;
                sec = time_seond % 60;

                if (number < 0)
                {
                    number = 0;
                }

                perform = (number / 20) * 100;


                g.num = number;
                g.answered = this.answered;
                g.right = this.right;
                g.Wrong = this.wrong;
                g.performmance = perform;
                g.time_min = min;
                g.time_sec = sec;

                g.name = this.name;
                g.gender = this.gender;

                this.Hide();
                g.Show();
            }
            min = time_seond / 60;
            sec = time_seond % 60;


            lblTime.Text = min.ToString() + " : " + sec.ToString() + " min.";
        }

        private void mainMaunBtn_Click(object sender, EventArgs e)
        {
            Check_Grade cg = new Check_Grade();


            timer1.Stop();
            time_seond = 120 - time_seond;
            min = time_seond / 60;
            sec = time_seond % 60;

            if (number < 0)
            {
                number = 0;
            }

            perform = (number / 20) * 100;

            cg.num = number;
            cg.answered = this.answered;
            cg.right = this.right;
            cg.Wrong = this.wrong;
            cg.performmance = perform;
            cg.time_min = min;
            cg.time_sec = sec;
            cg.category = lblCategory.Text;

            cg.name = this.name;
            cg.gender = this.gender;

            this.Hide();
            cg.Show();
        }


        bool digitSt(char cs)
        {
            int dd = cs;

            if (dd > 47 && dd < 58)
                return true;
            else
                return false;
        }

        bool validateSt(string ip)
        {
            Stack<char> ss = new Stack<char>();

            for (int i = 0; i < ip.Length; i++)
            {
                switch (ip[i])
                {
                    case '(': ss.Push(ip[i]); break;
                    case '{': ss.Push(ip[i]); break;
                    case '[': ss.Push(ip[i]); break;

                    case ')':
                        if (ss.Count != 0 && ss.Peek() == '(')
                            ss.Pop();
                        else
                            return false;
                        break;
                    case '}':
                        if (ss.Count != 0 && ss.Peek() == '{')
                            ss.Pop();
                        else
                            return false;
                        break;
                    case ']':
                        if (ss.Count != 0 && ss.Peek() == '[')
                            ss.Pop();
                        else
                            return false;
                        break;
                    default:
                        break;
                }

            }
            if (ss.Count == 0 && (digitSt(ip[ip.Length - 1]) || ip[ip.Length - 1] == ')' || ip[ip.Length - 1] == '}' || ip[ip.Length - 1] == ']'))
                return true;
            else
                return false;
        }

        int presidenceChk(char a, char b)
        {
            int m = 0, n = 0;
            if (a == b)
                return a;
            switch (a)
            {
                case '/': m = 4; break;
                case '*': m = 3; break;
                case '-': m = 2; break;
                case '+': m = 1; break;

            }
            switch (b)
            {
                case '/': n = 4; break;
                case '*': n = 3; break;
                case '-': n = 2; break;
                case '+': n = 1; break;

            }

            return (m - n);
        }

        bool brcop(char ip)
        {
            switch (ip)
            {
                case '(':
                case '{':
                case '[': return true;
                default: return false;
            }
        }
        bool brccl(char ip)
        {
            switch (ip)
            {
                case '}':
                case ']':
                case ')': return true;
                default: return false;
            }
        }



        public char opbr(char ch)
        {
            switch (ch)
            {
                case ')': return '(';
                case '}': return '{';
                case ']': return '[';
                default: return ch;

            }
        }

        string[] postfixNew(string ip)
        {
            int y = 0;
            string[] str = new string[ip.Length + 1];
            Stack<char> ch = new Stack<char>();
            Stack<string> ss = new Stack<string>();


            int l = 0;

            for (int i = 0; i < ip.Length; i++)
            {
                if (digitSt(ip[i]))
                    if (l == 0)
                    {
                        ss.Push(ip[i].ToString());
                        str[y++] = ss.Peek();
                        l = 1;
                    }
                    else
                    {

                        ss.Push(ss.Pop() + ip[i]);
                        str[y - 1] = ss.Peek();
                    }
                else
                {
                    l = 0;
                    if (brcop(ip[i]))
                        ch.Push(ip[i]);
                    else if (brccl(ip[i]))
                    {
                        while (ch.Peek() != opbr(ip[i]))
                        {
                            ss.Push("" + ch.Pop());
                            str[y++] = ss.Peek();
                        }
                        if (ch.Peek() == opbr(ip[i]))
                            ch.Pop();
                    }
                    else
                    {

                        if (ch.Count == 0)
                            ch.Push(ip[i]);
                        else
                        {
                            int x = 0;
                            while (ch.Count >= 1)
                            {
                                if (presidenceChk(ch.Peek(), ip[i]) > 0)
                                {
                                    ss.Push(ch.Pop().ToString());
                                    //ch.Push(ip[i]);
                                    str[y++] = ss.Peek();
                                    x = 1;

                                }
                                else
                                {
                                    ch.Push(ip[i]);
                                    x = 0;
                                    break;
                                }


                            }
                            if (x == 1)
                                ch.Push(ip[i]);

                        }
                    }

                }

            }
            while (ch.Count != 0)
            {
                ss.Push(ch.Pop().ToString());
                str[y++] = ss.Peek();
            }



            string[] stt = new string[ss.Count];
            y = ss.Count - 1;
            while (ss.Count != 0)
            {
                stt[y--] = ss.Pop();
            }

            return stt;
        }

        bool operato(string a)
        {
            if (a == "+" || a == "-" || a == "*" || a == "/")
                return true;
            else
                return false;
        }


        string evaluateNew(string ip)
        {
            string[] st;
            if (!validateSt(ip))
                return "syntex error";
            st = postfixNew(ip);
            float n1, n2;
            Stack<float> ss = new Stack<float>();

            for (int i = 0; i < st.Length; i++)
            {
                if (operato(st[i]))
                {
                    n2 = ss.Pop();
                    n1 = ss.Pop();
                    switch (st[i])
                    {
                        case "*": ss.Push(n1 * n2); break;
                        case "/": ss.Push(n1 / n2); break;
                        case "+": ss.Push(n1 + n2); break;
                        case "-": ss.Push(n1 - n2); break;
                        default: break;
                    }
                }
                else
                    ss.Push(float.Parse(st[i]));

            }

            if (ss.Peek().ToString() == "∞")
                return "math error";
            else
                return ss.Pop().ToString();
        }

        private void lblTime_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
