﻿namespace InfoBank2
{
    partial class Grammer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.lb_time = new System.Windows.Forms.Label();
            this.lb_quesLeft = new System.Windows.Forms.Label();
            this.lb_category = new System.Windows.Forms.Label();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.btn_skip = new System.Windows.Forms.Button();
            this.btn_submit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.questionTextBox = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mainMaunBtn = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblQusLft = new System.Windows.Forms.Label();
            this.lblCaterory = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_exit
            // 
            this.btn_exit.BackColor = System.Drawing.Color.MintCream;
            this.btn_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_exit.Font = new System.Drawing.Font("MINI 7 Condensed", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.ForeColor = System.Drawing.Color.Crimson;
            this.btn_exit.Location = new System.Drawing.Point(605, 373);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 36);
            this.btn_exit.TabIndex = 76;
            this.btn_exit.Text = "EXIT";
            this.btn_exit.UseVisualStyleBackColor = false;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.Color.MintCream;
            this.btn_start.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_start.Font = new System.Drawing.Font("MINI 7 Condensed", 12F);
            this.btn_start.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_start.Location = new System.Drawing.Point(609, 12);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 36);
            this.btn_start.TabIndex = 75;
            this.btn_start.Text = "START";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lb_time
            // 
            this.lb_time.AutoSize = true;
            this.lb_time.BackColor = System.Drawing.Color.MintCream;
            this.lb_time.Font = new System.Drawing.Font("Neuropol", 12F);
            this.lb_time.Location = new System.Drawing.Point(12, 19);
            this.lb_time.Name = "lb_time";
            this.lb_time.Size = new System.Drawing.Size(58, 18);
            this.lb_time.TabIndex = 72;
            this.lb_time.Text = "TIME :";
            // 
            // lb_quesLeft
            // 
            this.lb_quesLeft.AutoSize = true;
            this.lb_quesLeft.BackColor = System.Drawing.Color.MintCream;
            this.lb_quesLeft.Font = new System.Drawing.Font("Lucida Handwriting", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_quesLeft.Location = new System.Drawing.Point(13, 53);
            this.lb_quesLeft.Name = "lb_quesLeft";
            this.lb_quesLeft.Size = new System.Drawing.Size(135, 17);
            this.lb_quesLeft.TabIndex = 70;
            this.lb_quesLeft.Text = "QUESTIONS LEFT :";
            // 
            // lb_category
            // 
            this.lb_category.AutoSize = true;
            this.lb_category.BackColor = System.Drawing.Color.MintCream;
            this.lb_category.Font = new System.Drawing.Font("Tabitha", 11.25F);
            this.lb_category.Location = new System.Drawing.Point(339, 19);
            this.lb_category.Name = "lb_category";
            this.lb_category.Size = new System.Drawing.Size(93, 19);
            this.lb_category.TabIndex = 68;
            this.lb_category.Text = "CATEGORY :";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.radioButton4.Font = new System.Drawing.Font("Algerian", 12F);
            this.radioButton4.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton4.Location = new System.Drawing.Point(290, 277);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(93, 22);
            this.radioButton4.TabIndex = 66;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "OPTION 4";
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.radioButton3.Font = new System.Drawing.Font("Algerian", 12F);
            this.radioButton3.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton3.Location = new System.Drawing.Point(290, 243);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(93, 22);
            this.radioButton3.TabIndex = 65;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "OPTION 3";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.radioButton2.Font = new System.Drawing.Font("Algerian", 12F);
            this.radioButton2.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton2.Location = new System.Drawing.Point(290, 209);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(93, 22);
            this.radioButton2.TabIndex = 64;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "OPTION 2";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.radioButton1.Font = new System.Drawing.Font("Algerian", 12F);
            this.radioButton1.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton1.Location = new System.Drawing.Point(290, 177);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(93, 22);
            this.radioButton1.TabIndex = 63;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "OPTION 1";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // btn_skip
            // 
            this.btn_skip.BackColor = System.Drawing.Color.MintCream;
            this.btn_skip.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_skip.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_skip.Font = new System.Drawing.Font("MINI 7 Condensed", 12F);
            this.btn_skip.ForeColor = System.Drawing.Color.Red;
            this.btn_skip.Location = new System.Drawing.Point(187, 318);
            this.btn_skip.Name = "btn_skip";
            this.btn_skip.Size = new System.Drawing.Size(104, 35);
            this.btn_skip.TabIndex = 62;
            this.btn_skip.Text = "SKIP";
            this.btn_skip.UseVisualStyleBackColor = false;
            this.btn_skip.Visible = false;
            this.btn_skip.Click += new System.EventHandler(this.btn_skip_Click);
            // 
            // btn_submit
            // 
            this.btn_submit.BackColor = System.Drawing.Color.MintCream;
            this.btn_submit.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.btn_submit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_submit.Font = new System.Drawing.Font("MINI 7 Condensed", 12F);
            this.btn_submit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_submit.Location = new System.Drawing.Point(437, 318);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(113, 35);
            this.btn_submit.TabIndex = 61;
            this.btn_submit.Text = "SUBMIT";
            this.btn_submit.UseVisualStyleBackColor = false;
            this.btn_submit.Visible = false;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.label1.Font = new System.Drawing.Font("Amerika", 8.999999F);
            this.label1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label1.Location = new System.Drawing.Point(20, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 16);
            this.label1.TabIndex = 60;
            this.label1.Text = "QUESTION :";
            // 
            // questionTextBox
            // 
            this.questionTextBox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.questionTextBox.Font = new System.Drawing.Font("Aero", 12F);
            this.questionTextBox.ForeColor = System.Drawing.Color.Black;
            this.questionTextBox.Location = new System.Drawing.Point(104, 94);
            this.questionTextBox.Multiline = true;
            this.questionTextBox.Name = "questionTextBox";
            this.questionTextBox.ReadOnly = true;
            this.questionTextBox.Size = new System.Drawing.Size(576, 67);
            this.questionTextBox.TabIndex = 59;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mainMaunBtn
            // 
            this.mainMaunBtn.BackColor = System.Drawing.Color.Khaki;
            this.mainMaunBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mainMaunBtn.Font = new System.Drawing.Font("MINI 7 Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainMaunBtn.Location = new System.Drawing.Point(15, 356);
            this.mainMaunBtn.Name = "mainMaunBtn";
            this.mainMaunBtn.Size = new System.Drawing.Size(106, 53);
            this.mainMaunBtn.TabIndex = 77;
            this.mainMaunBtn.Text = "Main Manu";
            this.mainMaunBtn.UseVisualStyleBackColor = false;
            this.mainMaunBtn.Click += new System.EventHandler(this.mainMaunBtn_Click);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lblTime.Font = new System.Drawing.Font("MINI 7 Extended", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(82, 20);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(28, 13);
            this.lblTime.TabIndex = 78;
            this.lblTime.Text = "...";
            this.lblTime.Click += new System.EventHandler(this.lblTime_Click);
            // 
            // lblQusLft
            // 
            this.lblQusLft.AutoSize = true;
            this.lblQusLft.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lblQusLft.Font = new System.Drawing.Font("MINI 7 Extended", 11.25F);
            this.lblQusLft.Location = new System.Drawing.Point(154, 55);
            this.lblQusLft.Name = "lblQusLft";
            this.lblQusLft.Size = new System.Drawing.Size(34, 15);
            this.lblQusLft.TabIndex = 79;
            this.lblQusLft.Text = "...";
            this.lblQusLft.Click += new System.EventHandler(this.lblQusLft_Click);
            // 
            // lblCaterory
            // 
            this.lblCaterory.AutoSize = true;
            this.lblCaterory.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lblCaterory.Font = new System.Drawing.Font("MINI 7 Extended", 11.25F);
            this.lblCaterory.Location = new System.Drawing.Point(340, 53);
            this.lblCaterory.Name = "lblCaterory";
            this.lblCaterory.Size = new System.Drawing.Size(34, 15);
            this.lblCaterory.TabIndex = 80;
            this.lblCaterory.Text = "...";
            this.lblCaterory.Click += new System.EventHandler(this.lblCaterory_Click);
            // 
            // Grammer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(696, 419);
            this.Controls.Add(this.lblCaterory);
            this.Controls.Add(this.lblQusLft);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.mainMaunBtn);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.lb_time);
            this.Controls.Add(this.lb_quesLeft);
            this.Controls.Add(this.lb_category);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.btn_skip);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.questionTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Grammer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grammer";
            this.Load += new System.EventHandler(this.Grammer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label lb_time;
        private System.Windows.Forms.Label lb_quesLeft;
        private System.Windows.Forms.Label lb_category;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button btn_skip;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox questionTextBox;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button mainMaunBtn;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblQusLft;
        private System.Windows.Forms.Label lblCaterory;
    }
}