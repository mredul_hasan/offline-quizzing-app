﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfoBank2
{
    public partial class Category : Form
    {
        public string name;
        public string gender;


        General gn = new General();
        Mathematics math = new Mathematics();
        Programming pg = new Programming();
        Grammer gm = new Grammer();
        IQ iq = new IQ();
        public Category()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pg.name = this.name;
            pg.gender = this.gender;
            
            this.Hide();
            pg.Show();
        }

        private void btn_knowledge_Click(object sender, EventArgs e)
        {
            gn.name = this.name;
            gn.gender = this.gender;
            
            this.Hide();
            gn.Show();
        }

        private void btn_math_Click(object sender, EventArgs e)
        {
            math.name = this.name;
            math.gender = this.gender;
            
            
            this.Hide();
            math.Show();
        }

        private void btn_Gram_Click(object sender, EventArgs e)
        {
            gm.name = this.name;
            gm.gender = this.gender;
            
            
            this.Hide();
            gm.Show();
        }

        private void btn_iq_Click(object sender, EventArgs e)
        {
            iq.name = this.name;
            iq.gender = this.gender;
            
            this.Hide();
            iq.Show();
        }

        private void city_Click(object sender, EventArgs e)
        {
            city ct = new city();

            ct.name = this.name;
            ct.gender = this.gender;

            this.Hide();
            ct.Show();
        }

        private void Category_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
