﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfoBank2
{
    public partial class Check_Grade : Form
    {
        public double performmance;
        public double num;
        public int answered;
        public int right;
        public int Wrong;
        public int time_min;
        public int time_sec;
        public string category;

        public string name;
        public string gender;


        public Check_Grade()
        {
            InitializeComponent();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Check_Grade_Load(object sender, EventArgs e)
        {

        }

        private void yesBtn_Click(object sender, EventArgs e)
        {
            Gread g = new Gread();

            

            g.num = this.num;
            g.answered = this.answered;
            g.right = this.right;
            g.Wrong = this.Wrong;
            g.performmance = performmance;
            g.time_min = time_min;
            g.time_sec = time_sec;
            g.category = category;

            g.name = this.name;
            g.gender = this.gender;

            this.Hide();
            g.Show();
        }

        private void noBtn_Click(object sender, EventArgs e)
        {
            start_page sp = new start_page();

            sp.name = this.name;
            sp.gender = this.gender;

            this.Hide();
            sp.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
