﻿namespace InfoBank2
{
    partial class city
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_cat_show = new System.Windows.Forms.Label();
            this.lb_category = new System.Windows.Forms.Label();
            this.lb_QusLeft_show = new System.Windows.Forms.Label();
            this.lb_ques_left = new System.Windows.Forms.Label();
            this.lb_time_show = new System.Windows.Forms.Label();
            this.lb_time = new System.Windows.Forms.Label();
            this.btn_submit = new System.Windows.Forms.Button();
            this.btn_skip = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_start = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainMaunBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Meta-BoldCaps", 12F);
            this.radioButton3.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton3.Location = new System.Drawing.Point(516, 182);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(95, 23);
            this.radioButton3.TabIndex = 55;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "OPTION 3";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Meta-BoldCaps", 12F);
            this.radioButton2.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton2.Location = new System.Drawing.Point(516, 146);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(95, 23);
            this.radioButton2.TabIndex = 54;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "OPTION 2";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Meta-BoldCaps", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton1.Location = new System.Drawing.Point(516, 109);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(95, 23);
            this.radioButton1.TabIndex = 53;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "OPTION 1";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Font = new System.Drawing.Font("Meta-BoldCaps", 12F);
            this.radioButton4.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.radioButton4.Location = new System.Drawing.Point(516, 216);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(95, 23);
            this.radioButton4.TabIndex = 56;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "OPTION 4";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Meta-BoldCaps", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label7.Location = new System.Drawing.Point(107, 344);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 17);
            this.label7.TabIndex = 50;
            // 
            // lb_cat_show
            // 
            this.lb_cat_show.AutoSize = true;
            this.lb_cat_show.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lb_cat_show.Font = new System.Drawing.Font("MINI 7 Condensed", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_cat_show.Location = new System.Drawing.Point(304, 51);
            this.lb_cat_show.Name = "lb_cat_show";
            this.lb_cat_show.Size = new System.Drawing.Size(25, 15);
            this.lb_cat_show.TabIndex = 49;
            this.lb_cat_show.Text = "...";
            this.lb_cat_show.Click += new System.EventHandler(this.lb_cat_show_Click);
            // 
            // lb_category
            // 
            this.lb_category.AutoSize = true;
            this.lb_category.BackColor = System.Drawing.Color.MintCream;
            this.lb_category.Font = new System.Drawing.Font("Tabitha", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_category.Location = new System.Drawing.Point(303, 18);
            this.lb_category.Name = "lb_category";
            this.lb_category.Size = new System.Drawing.Size(86, 19);
            this.lb_category.TabIndex = 48;
            this.lb_category.Text = "CATEGORY";
            this.lb_category.Click += new System.EventHandler(this.lb_category_Click);
            // 
            // lb_QusLeft_show
            // 
            this.lb_QusLeft_show.AutoSize = true;
            this.lb_QusLeft_show.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lb_QusLeft_show.Font = new System.Drawing.Font("MINI 7 Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_QusLeft_show.Location = new System.Drawing.Point(124, 50);
            this.lb_QusLeft_show.Name = "lb_QusLeft_show";
            this.lb_QusLeft_show.Size = new System.Drawing.Size(35, 16);
            this.lb_QusLeft_show.TabIndex = 47;
            this.lb_QusLeft_show.Text = "...";
            this.lb_QusLeft_show.Click += new System.EventHandler(this.lb_QusLeft_show_Click);
            // 
            // lb_ques_left
            // 
            this.lb_ques_left.AutoSize = true;
            this.lb_ques_left.BackColor = System.Drawing.Color.MintCream;
            this.lb_ques_left.Font = new System.Drawing.Font("Aero", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ques_left.Location = new System.Drawing.Point(12, 50);
            this.lb_ques_left.Name = "lb_ques_left";
            this.lb_ques_left.Size = new System.Drawing.Size(105, 14);
            this.lb_ques_left.TabIndex = 46;
            this.lb_ques_left.Text = "QUESTIONS LEFT";
            this.lb_ques_left.Click += new System.EventHandler(this.lb_ques_left_Click);
            // 
            // lb_time_show
            // 
            this.lb_time_show.AutoSize = true;
            this.lb_time_show.BackColor = System.Drawing.Color.LightSeaGreen;
            this.lb_time_show.Font = new System.Drawing.Font("MINI 7 Extended", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_time_show.Location = new System.Drawing.Point(81, 17);
            this.lb_time_show.Name = "lb_time_show";
            this.lb_time_show.Size = new System.Drawing.Size(34, 15);
            this.lb_time_show.TabIndex = 45;
            this.lb_time_show.Text = "...";
            this.lb_time_show.Click += new System.EventHandler(this.lb_time_show_Click);
            // 
            // lb_time
            // 
            this.lb_time.AutoSize = true;
            this.lb_time.BackColor = System.Drawing.Color.MintCream;
            this.lb_time.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_time.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb_time.Font = new System.Drawing.Font("Neuropol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_time.Location = new System.Drawing.Point(12, 13);
            this.lb_time.Name = "lb_time";
            this.lb_time.Size = new System.Drawing.Size(52, 20);
            this.lb_time.TabIndex = 44;
            this.lb_time.Text = "TIME";
            this.lb_time.Click += new System.EventHandler(this.lb_time_Click);
            // 
            // btn_submit
            // 
            this.btn_submit.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.btn_submit.Font = new System.Drawing.Font("MINI 7 Condensed", 12F);
            this.btn_submit.Location = new System.Drawing.Point(516, 267);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(85, 34);
            this.btn_submit.TabIndex = 42;
            this.btn_submit.Text = "SUBMIT";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Visible = false;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // btn_skip
            // 
            this.btn_skip.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_skip.Font = new System.Drawing.Font("MINI 7 Condensed", 12F);
            this.btn_skip.Location = new System.Drawing.Point(516, 329);
            this.btn_skip.Name = "btn_skip";
            this.btn_skip.Size = new System.Drawing.Size(85, 34);
            this.btn_skip.TabIndex = 41;
            this.btn_skip.Text = "SKIP";
            this.btn_skip.UseVisualStyleBackColor = true;
            this.btn_skip.Visible = false;
            this.btn_skip.Click += new System.EventHandler(this.btn_skip_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btn_exit.Font = new System.Drawing.Font("MINI 7 Condensed", 12F);
            this.btn_exit.ForeColor = System.Drawing.Color.Crimson;
            this.btn_exit.Location = new System.Drawing.Point(718, 412);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 36);
            this.btn_exit.TabIndex = 40;
            this.btn_exit.Text = "EXIT";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_start
            // 
            this.btn_start.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_start.Font = new System.Drawing.Font("MINI 7 Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_start.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_start.Location = new System.Drawing.Point(718, 17);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 36);
            this.btn_start.TabIndex = 39;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(498, 291);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // mainMaunBtn
            // 
            this.mainMaunBtn.BackColor = System.Drawing.Color.Khaki;
            this.mainMaunBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mainMaunBtn.Font = new System.Drawing.Font("MINI 7 Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainMaunBtn.Location = new System.Drawing.Point(12, 408);
            this.mainMaunBtn.Name = "mainMaunBtn";
            this.mainMaunBtn.Size = new System.Drawing.Size(75, 40);
            this.mainMaunBtn.TabIndex = 59;
            this.mainMaunBtn.Text = "Main Manu";
            this.mainMaunBtn.UseVisualStyleBackColor = false;
            this.mainMaunBtn.Click += new System.EventHandler(this.mainMaunBtn_Click);
            // 
            // city
            // 
            this.AcceptButton = this.btn_submit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(805, 460);
            this.Controls.Add(this.mainMaunBtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lb_cat_show);
            this.Controls.Add(this.lb_category);
            this.Controls.Add(this.lb_QusLeft_show);
            this.Controls.Add(this.lb_ques_left);
            this.Controls.Add(this.lb_time_show);
            this.Controls.Add(this.lb_time);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.btn_skip);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_start);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "city";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "city";
            this.Load += new System.EventHandler(this.city_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lb_cat_show;
        private System.Windows.Forms.Label lb_category;
        private System.Windows.Forms.Label lb_QusLeft_show;
        private System.Windows.Forms.Label lb_ques_left;
        private System.Windows.Forms.Label lb_time_show;
        private System.Windows.Forms.Label lb_time;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Button btn_skip;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button mainMaunBtn;

    }
}