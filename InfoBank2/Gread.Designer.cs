﻿namespace InfoBank2
{
    partial class Gread
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Gread));
            this.performlvl = new System.Windows.Forms.Label();
            this.marklvl = new System.Windows.Forms.Label();
            this.anslvl = new System.Windows.Forms.Label();
            this.rightlvl = new System.Windows.Forms.Label();
            this.wronglvl = new System.Windows.Forms.Label();
            this.timelvl = new System.Windows.Forms.Label();
            this.performTimer = new System.Windows.Forms.Timer(this.components);
            this.mainMaunBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.certificateLvl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tnxlvl = new System.Windows.Forms.Label();
            this.sumitlvl = new System.Windows.Forms.Label();
            this.mredulLvl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // performlvl
            // 
            this.performlvl.AutoSize = true;
            this.performlvl.BackColor = System.Drawing.Color.Honeydew;
            this.performlvl.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.performlvl.Location = new System.Drawing.Point(516, 233);
            this.performlvl.Name = "performlvl";
            this.performlvl.Size = new System.Drawing.Size(92, 17);
            this.performlvl.TabIndex = 0;
            this.performlvl.Text = "performance";
            // 
            // marklvl
            // 
            this.marklvl.AutoSize = true;
            this.marklvl.BackColor = System.Drawing.Color.Honeydew;
            this.marklvl.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.marklvl.Location = new System.Drawing.Point(516, 252);
            this.marklvl.Name = "marklvl";
            this.marklvl.Size = new System.Drawing.Size(42, 17);
            this.marklvl.TabIndex = 1;
            this.marklvl.Text = "Mark";
            this.marklvl.Click += new System.EventHandler(this.marklvl_Click);
            // 
            // anslvl
            // 
            this.anslvl.AutoSize = true;
            this.anslvl.BackColor = System.Drawing.Color.Honeydew;
            this.anslvl.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.anslvl.Location = new System.Drawing.Point(516, 271);
            this.anslvl.Name = "anslvl";
            this.anslvl.Size = new System.Drawing.Size(74, 17);
            this.anslvl.TabIndex = 2;
            this.anslvl.Text = "Answered";
            // 
            // rightlvl
            // 
            this.rightlvl.AutoSize = true;
            this.rightlvl.BackColor = System.Drawing.Color.Honeydew;
            this.rightlvl.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.rightlvl.Location = new System.Drawing.Point(516, 291);
            this.rightlvl.Name = "rightlvl";
            this.rightlvl.Size = new System.Drawing.Size(45, 17);
            this.rightlvl.TabIndex = 3;
            this.rightlvl.Text = "Right";
            // 
            // wronglvl
            // 
            this.wronglvl.AutoSize = true;
            this.wronglvl.BackColor = System.Drawing.Color.Honeydew;
            this.wronglvl.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.wronglvl.Location = new System.Drawing.Point(516, 311);
            this.wronglvl.Name = "wronglvl";
            this.wronglvl.Size = new System.Drawing.Size(51, 17);
            this.wronglvl.TabIndex = 4;
            this.wronglvl.Text = "Wrong";
            // 
            // timelvl
            // 
            this.timelvl.AutoSize = true;
            this.timelvl.BackColor = System.Drawing.Color.Honeydew;
            this.timelvl.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.timelvl.Location = new System.Drawing.Point(516, 332);
            this.timelvl.Name = "timelvl";
            this.timelvl.Size = new System.Drawing.Size(41, 17);
            this.timelvl.TabIndex = 5;
            this.timelvl.Text = "Time";
            // 
            // performTimer
            // 
            this.performTimer.Enabled = true;
            this.performTimer.Interval = 500;
            this.performTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mainMaunBtn
            // 
            this.mainMaunBtn.BackColor = System.Drawing.Color.Khaki;
            this.mainMaunBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mainMaunBtn.Font = new System.Drawing.Font("MINI 7 Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainMaunBtn.Location = new System.Drawing.Point(449, 382);
            this.mainMaunBtn.Name = "mainMaunBtn";
            this.mainMaunBtn.Size = new System.Drawing.Size(75, 40);
            this.mainMaunBtn.TabIndex = 15;
            this.mainMaunBtn.Text = "Main Manu";
            this.mainMaunBtn.UseVisualStyleBackColor = false;
            this.mainMaunBtn.Click += new System.EventHandler(this.calculatebtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(710, 459);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // certificateLvl
            // 
            this.certificateLvl.BackColor = System.Drawing.Color.White;
            this.certificateLvl.Font = new System.Drawing.Font("Meta-BoldCaps", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.certificateLvl.Location = new System.Drawing.Point(80, 143);
            this.certificateLvl.Multiline = true;
            this.certificateLvl.Name = "certificateLvl";
            this.certificateLvl.ReadOnly = true;
            this.certificateLvl.Size = new System.Drawing.Size(584, 76);
            this.certificateLvl.TabIndex = 17;
            this.certificateLvl.TextChanged += new System.EventHandler(this.certificateLvl_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Honeydew;
            this.label1.Font = new System.Drawing.Font("Amerika", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(410, 233);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "performance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Honeydew;
            this.label2.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.label2.Location = new System.Drawing.Point(410, 252);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Mark";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Honeydew;
            this.label3.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.label3.Location = new System.Drawing.Point(410, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Answered";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Honeydew;
            this.label4.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.label4.Location = new System.Drawing.Point(410, 291);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 17);
            this.label4.TabIndex = 21;
            this.label4.Text = "Right";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Honeydew;
            this.label5.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.label5.Location = new System.Drawing.Point(410, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 22;
            this.label5.Text = "Wrong";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Honeydew;
            this.label6.Font = new System.Drawing.Font("Amerika", 9.75F);
            this.label6.Location = new System.Drawing.Point(410, 332);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 17);
            this.label6.TabIndex = 23;
            this.label6.Text = "Time";
            // 
            // tnxlvl
            // 
            this.tnxlvl.AutoSize = true;
            this.tnxlvl.BackColor = System.Drawing.Color.White;
            this.tnxlvl.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tnxlvl.Location = new System.Drawing.Point(156, 348);
            this.tnxlvl.Name = "tnxlvl";
            this.tnxlvl.Size = new System.Drawing.Size(170, 26);
            this.tnxlvl.TabIndex = 24;
            this.tnxlvl.Text = "Special Thanks From";
            // 
            // sumitlvl
            // 
            this.sumitlvl.AutoSize = true;
            this.sumitlvl.BackColor = System.Drawing.Color.Honeydew;
            this.sumitlvl.Font = new System.Drawing.Font("Meta-BoldCaps", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sumitlvl.Location = new System.Drawing.Point(118, 384);
            this.sumitlvl.Name = "sumitlvl";
            this.sumitlvl.Size = new System.Drawing.Size(90, 38);
            this.sumitlvl.TabIndex = 25;
            this.sumitlvl.Text = "Sumit Saha\r\n  Creator";
            // 
            // mredulLvl
            // 
            this.mredulLvl.AutoSize = true;
            this.mredulLvl.BackColor = System.Drawing.Color.Honeydew;
            this.mredulLvl.Font = new System.Drawing.Font("Meta-BoldCaps", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mredulLvl.Location = new System.Drawing.Point(267, 384);
            this.mredulLvl.Name = "mredulLvl";
            this.mredulLvl.Size = new System.Drawing.Size(110, 38);
            this.mredulLvl.TabIndex = 26;
            this.mredulLvl.Text = "Mredul Hasan\r\n    Creator";
            // 
            // Gread
            // 
            this.AcceptButton = this.mainMaunBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(715, 466);
            this.Controls.Add(this.mredulLvl);
            this.Controls.Add(this.sumitlvl);
            this.Controls.Add(this.tnxlvl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.certificateLvl);
            this.Controls.Add(this.mainMaunBtn);
            this.Controls.Add(this.timelvl);
            this.Controls.Add(this.wronglvl);
            this.Controls.Add(this.rightlvl);
            this.Controls.Add(this.anslvl);
            this.Controls.Add(this.marklvl);
            this.Controls.Add(this.performlvl);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Gread";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gread ";
            this.Load += new System.EventHandler(this.Gread_A_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label performlvl;
        private System.Windows.Forms.Label marklvl;
        private System.Windows.Forms.Label anslvl;
        private System.Windows.Forms.Label rightlvl;
        private System.Windows.Forms.Label wronglvl;
        private System.Windows.Forms.Label timelvl;
        private System.Windows.Forms.Timer performTimer;
        private System.Windows.Forms.Button mainMaunBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox certificateLvl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label tnxlvl;
        private System.Windows.Forms.Label sumitlvl;
        private System.Windows.Forms.Label mredulLvl;
    }
}