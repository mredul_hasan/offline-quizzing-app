﻿namespace InfoBank2
{
    partial class Category
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Category));
            this.btn_knowledge = new System.Windows.Forms.Button();
            this.btn_prog = new System.Windows.Forms.Button();
            this.btn_math = new System.Windows.Forms.Button();
            this.btn_Gram = new System.Windows.Forms.Button();
            this.btn_iq = new System.Windows.Forms.Button();
            this.city = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_knowledge
            // 
            this.btn_knowledge.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btn_knowledge.Font = new System.Drawing.Font("MINI 7 Tight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_knowledge.Location = new System.Drawing.Point(58, 68);
            this.btn_knowledge.Name = "btn_knowledge";
            this.btn_knowledge.Size = new System.Drawing.Size(206, 54);
            this.btn_knowledge.TabIndex = 0;
            this.btn_knowledge.Text = "General Knowledge";
            this.btn_knowledge.UseVisualStyleBackColor = false;
            this.btn_knowledge.Click += new System.EventHandler(this.btn_knowledge_Click);
            // 
            // btn_prog
            // 
            this.btn_prog.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btn_prog.Font = new System.Drawing.Font("MINI 7 Tight", 12F);
            this.btn_prog.Location = new System.Drawing.Point(425, 68);
            this.btn_prog.Name = "btn_prog";
            this.btn_prog.Size = new System.Drawing.Size(206, 54);
            this.btn_prog.TabIndex = 1;
            this.btn_prog.Text = "Programming";
            this.btn_prog.UseVisualStyleBackColor = false;
            this.btn_prog.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_math
            // 
            this.btn_math.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btn_math.Font = new System.Drawing.Font("MINI 7 Tight", 12F);
            this.btn_math.Location = new System.Drawing.Point(58, 210);
            this.btn_math.Name = "btn_math";
            this.btn_math.Size = new System.Drawing.Size(206, 54);
            this.btn_math.TabIndex = 2;
            this.btn_math.Text = "Mathematics";
            this.btn_math.UseVisualStyleBackColor = false;
            this.btn_math.Click += new System.EventHandler(this.btn_math_Click);
            // 
            // btn_Gram
            // 
            this.btn_Gram.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btn_Gram.Font = new System.Drawing.Font("MINI 7 Tight", 12F);
            this.btn_Gram.Location = new System.Drawing.Point(425, 210);
            this.btn_Gram.Name = "btn_Gram";
            this.btn_Gram.Size = new System.Drawing.Size(206, 54);
            this.btn_Gram.TabIndex = 3;
            this.btn_Gram.Text = "Grammer";
            this.btn_Gram.UseVisualStyleBackColor = false;
            this.btn_Gram.Click += new System.EventHandler(this.btn_Gram_Click);
            // 
            // btn_iq
            // 
            this.btn_iq.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btn_iq.Font = new System.Drawing.Font("MINI 7 Tight", 12F);
            this.btn_iq.Location = new System.Drawing.Point(58, 334);
            this.btn_iq.Name = "btn_iq";
            this.btn_iq.Size = new System.Drawing.Size(206, 54);
            this.btn_iq.TabIndex = 4;
            this.btn_iq.Text = "Intelligence Quotient";
            this.btn_iq.UseVisualStyleBackColor = false;
            this.btn_iq.Click += new System.EventHandler(this.btn_iq_Click);
            // 
            // city
            // 
            this.city.BackColor = System.Drawing.Color.LightSeaGreen;
            this.city.Font = new System.Drawing.Font("MINI 7 Tight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.city.Location = new System.Drawing.Point(425, 334);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(206, 54);
            this.city.TabIndex = 5;
            this.city.Text = "NAME IT !";
            this.city.UseVisualStyleBackColor = false;
            this.city.Click += new System.EventHandler(this.city_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(270, 159);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(149, 172);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Category
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(661, 422);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.city);
            this.Controls.Add(this.btn_iq);
            this.Controls.Add(this.btn_Gram);
            this.Controls.Add(this.btn_math);
            this.Controls.Add(this.btn_prog);
            this.Controls.Add(this.btn_knowledge);
            this.ForeColor = System.Drawing.Color.MidnightBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Category";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Category";
            this.Load += new System.EventHandler(this.Category_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_knowledge;
        private System.Windows.Forms.Button btn_prog;
        private System.Windows.Forms.Button btn_math;
        private System.Windows.Forms.Button btn_Gram;
        private System.Windows.Forms.Button btn_iq;
        private System.Windows.Forms.Button city;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}