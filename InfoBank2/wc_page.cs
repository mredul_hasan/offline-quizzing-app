﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfoBank2
{
    public partial class wc_page : Form
    {

        start_page sp = new start_page();
        
       // public string name,gendar;
        public wc_page()
        {
            InitializeComponent();
        }

        private void nameANDgendar_Load(object sender, EventArgs e)
        {
            
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            if (nameTextbox.Text == " " || nameTextbox.Text == "" && radioButtonMale.Checked == false && radioButtonFemale.Checked == false)
                MessageBox.Show("Please,Enter Your Name and Select Your Gender!\n");
            else if (radioButtonMale.Checked==false && radioButtonFemale.Checked==false)
                MessageBox.Show("Please,Select Your Gendar !");
            else if (nameTextbox.Text == " " || nameTextbox.Text == "")
                MessageBox.Show("Please,Enter Your Name !");
            else
            {
                sp.name = nameTextbox.Text;

                if (radioButtonFemale.Checked) 
                    sp.gender = "Female";
                else 
                    sp.gender = "Male";
                this.Hide();
                sp.Show();
            }

            
        }
    }
}
